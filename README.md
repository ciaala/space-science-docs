# Awesome Space Science List

## Engines


## Spaceships

## Rockets

- [Falcon 9](https://en.wikipedia.org/wiki/Falcon_9#Block_5)
- 

### Concepts

- [Project Longshot](https://en.wikipedia.org/wiki/Project_Longshot)
- [Fusion Rocket](https://en.wikipedia.org/wiki/Fusion_rocket)
- [Fusor](https://en.wikipedia.org/wiki/Fusor)

## Telescopes & Probes

- [Space Interferometry Mission](https://en.wikipedia.org/wiki/Space_Interferometry_Mission)

## Star systems

- [Around the Solar System](https://en.wikipedia.org/wiki/List_of_nearest_stars_and_brown_dwarfs) 
 
### Alpha Centauri

- [Wikipedia Alpha Centauri](https://en.wikipedia.org/wiki/Alpha_Centauri)
- [Proxima Centuri b - extra solar system confirmed planet](https://en.wikipedia.org/wiki/Proxima_Centauri_b)
 
## Galaxies


## Type of Star Body   